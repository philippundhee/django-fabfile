from setuptools import setup, find_packages

setup(
    name='django-fabfile',
    version='0.1',
    packages=['django_fabfile'],
    package_data={
        'django_fabfile': [
            'templates/*.template',
        ]
    },
    url='https://bitbucket.org/philippundhee/django-fabfile',
    license='',
    author='Philipp & Hee',
    author_email='info@philippundhee.ch',
    description='Fabfile for a complete django development process',
    install_requires=[
        "Fabric==1.8.0",
        "fabtools==0.16.0",
        "Django==1.5",
        "GrandFatherSon==1.2",
    ]
)
