"""
Creates and extracts a tar file from the backup
"""
import tarfile
from fabric.colors import green


def create_tar_file(arc_name, file_path, tar_path):
    """
    Creates a tar from a given file

    @param arc_name: Name of the file inside the tar
    @param file_path: Path to the file which should be included in the tar
    @param tar_path: Path to the tar
    """
    print(green('Create tar file {}'.format(arc_name), bold=True))
    tar = tarfile.open(tar_path, 'w:gz')
    tar.add(file_path, arcname=arc_name)
    tar.close()


def extract_tar_file(target_path, tar_path):
    """
    Extract from a given tar file

    @param tar_path: Path to extract the content of the tar
    @param target_path: Path to the tar file
    """
    print(green('Extract tar file', bold=True))
    tar = tarfile.open(tar_path, 'r:gz')
    tar.extractall(target_path)
