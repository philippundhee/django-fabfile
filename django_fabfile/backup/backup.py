"""
Backups the db and media files
"""
from django_fabfile.backup.compression import create_tar_file
from django_fabfile.backup.encryption import encrypt_file
from django_fabfile.backup.ftp import get_ftp_connection, create_ftp_directory_if_not_exists, navigate_to_ftp_directory, upload_file, clean_up_backups
from django_fabfile.db.postgres import pg_dump_db
from django_fabfile.utils.files import create_dir_if_not_exists, delete_all_files_in_directory, get_timestamped_filename
from django_fabfile.utils.git import assert_correct_branch
from django_fabfile.utils.settings import get_db_settings, get_key, get_local_backup_path, get_local_media_path
from fabric.colors import red
from fabric.state import env
from fabric.utils import abort
import os


def backup(source_branch, with_media, with_db):
    """
    Backups a branch.

    @param source_branch: Branch to backup
    @param with_media: Boolean stating if media files should be backuped
    @param with_db: Boolean stating if db should be backuped
    """
    assert_correct_branch(source_branch)
    if not (with_media or with_db):
        abort(red('Neither DB nor media restore'))

    # local backup path
    local_backup_path = get_local_backup_path()
    create_dir_if_not_exists(local_backup_path)

    # backup DB
    if with_db:
        backup_db(source_branch, local_backup_path)

    # backup media
    if with_media:
        backup_media(source_branch, local_backup_path)

    # Delete temporary files
    delete_all_files_in_directory(local_backup_path)


def backup_db(source_branch, local_backup_path):
    """
    Takes care of the DB backup process

    @param source_branch: Branch defining the DB
    @param local_backup_path: Path for the backup
    """
    # Paths
    timestamp = get_timestamped_filename(env['project_settings']['project_name'],
                                         env['project_settings']['db_type_filename'], source_branch)
    db_dump_filename = '{filename}{suffix}'.format(filename=timestamp, suffix=env['project_settings']['pgdump_suffix'])
    db_dump_path = os.path.join(local_backup_path, db_dump_filename)

    # DB Dump
    db_settings = get_db_settings(source_branch)
    pg_dump_db(db_settings['username'], db_settings['password'], db_settings['database'], db_dump_path)

    # Compress, encrypt and upload file to FTP server
    compress_encrypt_upload_file(db_dump_filename, db_dump_path, local_backup_path, source_branch,
                                 env['project_settings']['db_type_filename'])


def backup_media(source_branch, local_backup_path):
    """
    Takes care of the backup of the media files

    @param source_branch: Branch to backup
    @param local_backup_path: Path for the backup
    """
    # Paths
    media_dump_filename = get_timestamped_filename(env['project_settings']['project_name'],
                                                   env['project_settings']['media_type_filename'], source_branch)
    media_path = get_local_media_path()
    create_dir_if_not_exists(media_path)

    # Compress, encrypt and upload file to FTP server
    compress_encrypt_upload_file(media_dump_filename, media_path, local_backup_path, source_branch,
                                 env['project_settings']['media_type_filename'])


def compress_encrypt_upload_file(filename, path, local_backup_path, source_branch, sub_directory):
    """
    Compresses, encrypts and uploads a backup file to the FTP server
    @param filename: Name of the file to be processed
    @param path: Path of the file to be processed
    @param local_backup_path: Path to the local directory
    @param source_branch: Branch
    @param sub_directory: Subdirectory on the FTP Server. E.g. db or media
    """
    # filenames and paths
    tar_filename = '{}{}'.format(filename, env['project_settings']['tar_gz_suffix'])
    tar_path = os.path.join(local_backup_path, tar_filename)
    encrypted_filename = tar_filename + env['project_settings']['aes_suffix']
    encrypted_path = os.path.join(local_backup_path, encrypted_filename)

    # tar.gz both DB and media folder
    create_tar_file(filename, path, tar_path)

    # Encrypt tar with key
    encrypt_file(encrypted_path, tar_path, get_key())

    # Get FTP connection
    ftp_connection = get_ftp_connection()

    # Create FTP folder for project if not exists
    create_ftp_directory_if_not_exists(ftp_connection, env['project_settings']['project_name'])

    # Navigate to project folder on FTP server
    navigate_to_ftp_directory(ftp_connection, env['project_settings']['project_name'])

    # Create FTP folder for branch if not exists
    create_ftp_directory_if_not_exists(ftp_connection, source_branch)

    # Navigate to branch folder
    navigate_to_ftp_directory(ftp_connection, source_branch)

    # Create subfolder inside branch if not exists
    create_ftp_directory_if_not_exists(ftp_connection, sub_directory)

    # Navigate to subfolder folder
    navigate_to_ftp_directory(ftp_connection, sub_directory)

    # Upload encrypted file to FTP
    upload_file(encrypted_filename, encrypted_path, ftp_connection)

    # Delete old backups
    clean_up_backups(ftp_connection)

    # Close FTP Connection
    ftp_connection.quit()