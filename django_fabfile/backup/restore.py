"""
Restores the db and media files
"""
from django_fabfile.backup.compression import extract_tar_file
from django_fabfile.backup.encryption import decrypt_file
import re
from django_fabfile.backup.ftp import get_ftp_connection, navigate_to_ftp_directory, get_latest_backup, download_file
from django_fabfile.db.postgres import pg_restore_db
from django_fabfile.utils.files import create_dir_if_not_exists, delete_all_files_in_directory, move_all_files_in_directory
from django_fabfile.utils.git import assert_correct_branch, is_on_accpt_branch, is_on_prod_branch
from django_fabfile.utils.settings import get_db_settings, get_key, get_local_backup_path, get_local_media_path
from fabric.colors import red, blue, green
from fabric.contrib.console import confirm
from fabric.state import env
from fabric.utils import abort
import os


def restore(source_branch, target_branch, with_media, with_db):
    """
    Restores a branch

    @param source_branch: Branch from which the dump should be taken
    @param target_branch: Branch which content should be replaced
    @param with_media: Boolean stating if media files should be restored
    @param with_db: Boolean stating if DB should be restored
    """
    assert_correct_branch(target_branch)
    if not (with_media or with_db):
        abort(red('Neither DB nor media restore'))

    # if is_on_accpt_branch() or is_on_prod_branch():
    #     if not confirm(blue(
    #             'You are restoring the database and media folder on the {branch} server. This can lead to data loss! Do you want to continue?'.format(
    #                     branch=target_branch), bold=True)):
    #         abort(red('Abortion by user'))

    # All paths
    ftp_project_path = os.path.join(env['project_settings']['project_name'], source_branch)
    local_backup_path = get_local_backup_path()
    create_dir_if_not_exists(local_backup_path)


    # DB Dump
    ftp_db_path = os.path.join(ftp_project_path, env['project_settings']['db_type_filename'])
    restore_db(ftp_db_path, local_backup_path, source_branch, target_branch)

    # Media
    if with_media:
        ftp_media_path = os.path.join(ftp_project_path, env['project_settings']['media_type_filename'])
        restore_media(ftp_media_path, local_backup_path, source_branch)

    # Delete temporary files
    delete_all_files_in_directory(local_backup_path)


def restore_db(ftp_project_path, local_backup_path, source_branch, target_branch):
    """
    Restores the DB

    @param ftp_project_path: Path on the FTP Server to the dump directory
    @param local_backup_path: Path to the local backup path
    @param source_branch: Branch from which the dump should be taken
    @param target_branch: Branch defines the DB to be replaced
    """
    # Download File
    path = download_decrypt_decompress_file(ftp_project_path, local_backup_path, source_branch)

    db_settings = get_db_settings(target_branch)

    # Restore DB
    pg_restore_db(db_settings['username'], db_settings['password'], db_settings['database'], path)


def restore_media(ftp_project_path, local_backup_path, source_branch):
    """
    Restores the media files

    @param ftp_project_path: Path on the FTP Server to the dump directory
    @param local_backup_path: Path to the local backup path
    @param source_branch: Branch from which the dump should be taken
    @return:
    """
    media_path = get_local_media_path()

    # Download File
    path = download_decrypt_decompress_file(ftp_project_path, local_backup_path, source_branch)

    # Clean the media directory
    delete_all_files_in_directory(media_path)

    # Move media files to media directory
    print(green('Move media files', bold=True))
    create_dir_if_not_exists(media_path)
    move_all_files_in_directory(media_path, path)


def download_decrypt_decompress_file(ftp_project_path, local_backup_path, source_branch):
    """
    Downloads, decrypts and decompresses a file from a FTP server

    @param ftp_project_path: Path on the FTP Server to the dump directory
    @param local_backup_path: Path to the local backup path
    @param source_branch: Branch from which the dump should be taken
    @return: Path to the downloaded file
    """
    # Get FTP connection
    ftp_connection = get_ftp_connection()

    # Navigate to directory
    navigate_to_ftp_directory(ftp_connection, ftp_project_path)

    # Get filename of latest backup
    encrypted_filename = get_latest_backup(ftp_connection)

    # Construct paths
    encrypted_path = os.path.join(local_backup_path, encrypted_filename)
    tar_path = re.sub('\{}$'.format(env['project_settings']['aes_suffix']), '', encrypted_path)

    # Download file
    download_file(encrypted_filename, encrypted_path, ftp_connection)

    # Decrypt file
    decrypt_file(encrypted_path, tar_path, get_key())

    # Extract the tar to temp directory
    extract_tar_file(local_backup_path, tar_path)

    path = re.sub('\{}$'.format(env['project_settings']['tar_gz_suffix']), '', tar_path)
    return path
