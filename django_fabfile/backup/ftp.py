"""
All methods required for the upload and download of the backups to a FTP server
"""
import ftplib
import datetime
from collections import Counter as _Counter

from django_fabfile.utils.files import extract_date_from_filename
from fabric.colors import red, green
from fabric.state import env
from fabric.utils import abort
from grandfatherson import to_delete as _to_delete


def get_ftp_connection():
    """
    Creates a FTP connection with the settings given in the env variable

    @return: The created ftp_connection
    """
    ftp_connection = ftplib.FTP(env['project_settings']['ftp_credentials']['host'])
    ftp_connection.login(env['project_settings']['ftp_credentials']['user'],
                         env['project_settings']['ftp_credentials']['password'])
    return ftp_connection


def create_ftp_directory_if_not_exists(ftp_connection, directory_name):
    """
    Creates a new directory on the FTP server if not existing

    @param ftp_connection: FTP connection to server
    @param directory_name: Directory to be created
    """
    try:
        ftp_connection.mkd(directory_name)
    except ftplib.error_perm:
        pass


def navigate_to_ftp_directory(ftp_connection, directory_name):
    """
    Navigates to given ftp directory

    @param ftp_connection: FTP connection to server
    @param directory_name: Directory to navigate
    """
    try:
        ftp_connection.cwd(directory_name)
    except ftplib.error_perm:
        abort(red('No directory "{}" found on FTP server'.format(directory_name), bold=True))


def upload_file(filename, path, ftp_connection):
    """
    Uplaods file to FTP server

    @param filename: File name on server
    @param path: Local path to file
    @param ftp_connection: FTP connection to server
    """
    print(green('Upload file {} to ftp server'.format(filename), bold=True))
    try:
        file_ = open(path, 'rb')
        ftp_connection.storbinary('STOR {file}'.format(file=filename), file_)
        file_.close()
    except Exception:
        abort(red('File "{}" could not be uploaded to FTP server'.format(filename), bold=True))


def download_file(filename, path, ftp_connection):
    """
    Download file from FTP server

    @param filename: File name on server
    @param path: Local path to file
    @param ftp_connection: FTP connection to server
    """
    print(green('Download file {} from ftp server'.format(filename), bold=True))
    try:
        file_ = open(path, 'wb')
        ftp_connection.retrbinary('RETR {file}'.format(file=filename), file_.write)
        file_.close()
    except Exception:
        abort(red('File "{}" could not be downloaded from FTP server'.format(filename), bold=True))


def clean_up_backups(ftp_connection):
    """
    Deletes old backups on server

    @param ftp_connection: FTP connection to server
    """
    print(green('Clean up backups', bold=True))
    files = ftp_connection.nlst()
    creation_dates = []
    file_dict = {}
    for filename in files:
        creation_date = extract_date_from_filename(filename)
        creation_dates.append(creation_date)
        file_dict[creation_date] = filename
    dates_to_delete = _to_delete(creation_dates, minutes=1, hours=1, days=3, weeks=3, months=2, years=2,
                                 now=datetime.datetime.now())
    for date_to_delete in dates_to_delete:
        file_to_delete = file_dict[date_to_delete]
        ftp_connection.delete(file_to_delete)
        print(green('Delete old backup "{file_to_delete}"'.format(file_to_delete=file_to_delete), bold=True))


def get_latest_backup(ftp_connection):
    """
    Gets the latest backup found on the FTP server

    @param ftp_connection: FTP connection to server
    @return:
    """
    encrypted_filename = ''
    files = ftp_connection.nlst()
    sorted_files = _Counter()
    for filename in files:
        creation_date = extract_date_from_filename(filename)
        sorted_files[filename] = creation_date
    if len(sorted_files) > 0:
        encrypted_filename = sorted_files.most_common(1)[0][0]
    else:
        abort(red('No backup found!', bold=True))
    print(green('Following backup has been taken: {}'.format(encrypted_filename), bold=True))
    return encrypted_filename