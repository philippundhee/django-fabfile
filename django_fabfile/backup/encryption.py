"""
Encrypt and decrypt a given file
"""
from fabric.colors import green
from fabric.context_managers import hide
from fabric.api import local


def encrypt_file(target_path, origin_path, key):
    """
    Encrypt a given file with openssl

    @param target_path: Path with the encrypted file
    @param origin_path: Path to the file to be encrypted
    @param key: Password for the encryption
    """
    key = _escape_key(key)
    print(green('Encrypt file', bold=True))
    with hide('running', 'stdout', 'stderr'):
        local('openssl aes-256-cbc -salt -in {origin_file} -out {encrypted_file} -pass pass:"{key}"'.format(
            origin_file=origin_path, encrypted_file=target_path, key=key))


def decrypt_file(origin_path, target_path, key):
    """
    Decrypt a given file with openssl

    @param origin_path:
    @param target_path:
    @param key:
    """
    key = _escape_key(key)
    print(green('Decrypt file', bold=True))
    with hide('running', 'stdout', 'stderr'):
        local('openssl aes-256-cbc -d -salt -in {origin_file} -out {encrypted_file} -pass pass:"{key}"'.format(
            origin_file=origin_path,
            encrypted_file=target_path,
            key=key))


def _escape_key(key):
    """
    Escape key
    @param key: original key
    @return: escaped key
    """
    key = ''.join(e for e in key if e.isalnum())
    return key
