"""
Helper methods to run operations either local or on server
"""
import getpass
import json
import os
from django_fabfile.utils.settings import get_remote_path, get_local_project_path, get_server_user, get_local_path, get_remote_project_path
from fabric.colors import red, green
from fabric.state import env
from fabric.utils import abort
from fabric.api import local, settings
from fabric.context_managers import lcd, cd, hide
from fabric.operations import run, sudo
import fabtools


def abort_if_is_on_server(is_on_server):
    """
    Aborts if boolean is_on_server is true

    @param is_on_server: Boolean to check
    """
    if is_on_server:
        abort(red('You are on the dev branch on the server!'))


def set_remote_executable_permission(path):
    """
    Gives executable permission to a remote file

    @param path: Path to the file
    """
    with settings(user=get_server_user()):
        run('chmod +x {path}'.format(path=path))


def run_remote_bootstrap(source_branch):
    """
    Runs the remote bootstrap file

    @param source_branch: Branch on the server
    """
    with settings(user=get_server_user()):
        with cd(get_remote_path(source_branch)):
            set_remote_executable_permission('bootstrap')
            run('./bootstrap {branch}'.format(branch=source_branch))


def run_remote_fab_command(command, source_branch):
    """
    Runs a fab command on the server

    @param command: Fab command to execute
    @param source_branch: Branch on the server
    """
    with settings(user=get_server_user()):
        with cd(get_remote_path(source_branch)):
            with fabtools.python.virtualenv('.'):
                run('fab {command}'.format(command=command))


def create_server_user_if_not_exists(root_password, server_user):
    """
    Creates a new server user if not yet existing

    @param root_password: Password of the root server
    @param server_user: Name of the server user
    """
    with settings(user='root', password=root_password):
        if not fabtools.user.exists(server_user):
            ssh_public_keys_path = os.path.join(get_local_path(), 'ssh_public_keys')
            password = getpass.getpass('Please enter password for server user "{}": '.format(server_user))
            with settings(hide('running', 'stdout')):
                fabtools.user.create(name=server_user, create_home=True,
                                     group=server_user, password=password,
                                     ssh_public_keys=ssh_public_keys_path, shell='/bin/bash')
            print(green('User "{server_user}" created.'.format(server_user=server_user)))
        else:
            print(green('User "{server_user}" already exists on server.'.format(server_user=server_user)))

        env['project_settings']['home_path'] = fabtools.user.home_directory(server_user)


def get_deploy_key():
    """
    Creates (if not existing) and gets the ssh public key of the server

    @return: ssh public key
    """
    with settings(user=get_server_user()):
        run('test -d ~/.ssh || mkdir ~/.ssh')
        run('test -f ~/.ssh/id_rsa.pub || ssh-keygen -qt rsa -f ~/.ssh/id_rsa -P ""')
        return run('cat ~/.ssh/id_rsa.pub')


def add_deploy_key_to_bitbucket(bitbucket_password):
    """
    Adds the ssh public key of the server to the deploy keys of the bitbucket repository (if not existing)

    @param bitbucket_password: Password for the bitbucket user
    """
    deploy_key = get_deploy_key()
    with settings(hide('running', 'stdout', 'stderr', 'warnings')):
        current_deployment_keys = json.loads(local(
            'curl -X GET -u {bitbucket_user}:{bitbucket_password} https://bitbucket.org/api/1.0/repositories/{bitbucket_user}/{project_name}/deploy-keys'.format(
                bitbucket_user=env['project_settings']['bitbucket_user'], bitbucket_password=bitbucket_password,
                project_name=env['project_settings']['project_name']), capture=True))
        if not any(deploy_key in current_key['key'] for current_key in current_deployment_keys):
            local(
                'curl -X POST -u {bitbucket_user}:{bitbucket_password} https://bitbucket.org/api/1.0/repositories/{bitbucket_user}/{project_name}/deploy-keys --data-urlencode "key={key}" --data-urlencode "label=Server"'.format(
                    bitbucket_user=env['project_settings']['bitbucket_user'], bitbucket_password=bitbucket_password,
                    project_name=env['project_settings']['project_name'], key=deploy_key))
            print(green('Posted deployment key on Bitbucket.'))
        else:
            print(green('Deployment key already on Bitbucket.'))


def run_local_manage(branch, command):
    """
    Runs a python manage.py command with the correct settings file

    @param branch: Branch to choose correct settings file
    @param command: Command to execute
    """
    with lcd(get_local_project_path()):
        local('python manage.py {command} --settings={project_name}.settings.{branch}'.format(
            project_name=env['project_settings']['project_name'], branch=branch, command=command))


def run_remote_manage(branch, command):
    """
    Runs a python manage.py command with the correct settings file on the server

    @param branch: Branch to choose correct settings file
    @param command: Command to execute
    """
    with settings(user=get_server_user(), warn_only=True):
        with cd(get_remote_project_path(branch)):
            with fabtools.python.virtualenv('../.'):
                run('python manage.py {command} --settings={project_name}.settings.{branch}'.format(
                    project_name=env['project_settings']['project_name'], branch=branch, command=command))


def reload_nginx(root_password):
    """
    Reloads the nginx server

    @param root_password: Password for the root user
    """
    with settings(user='root', password=root_password):
        if sudo('nginx -t').return_code == 0:
            sudo('nginx -s reload')
            print(green('Nginx reloaded'))
        else:
            print(red('Error in nginx conf'))


def reload_supervisor(root_password):
    """
    Reloads the supvervisor daemon

    @param root_password: Password for the root user
    """
    with settings(user='root', password=root_password):
        sudo('supervisorctl update')
        print(green('Supervisor reloaded'))


def restart_gunicorn(branch, root_password):
    """
    Restart supervisor process (gunicorn)

    @param branch: Branch
    @param root_password: Password of the server root user
    """
    with settings(user='root', password=root_password):
        run('supervisorctl restart {project_name}_{branch}_gunicorn'.format(
            project_name=env['project_settings']['project_name'], branch=branch))


def restart_huey(branch, root_password):
    """
    Restart supervisor process (gunicorn)

    @param branch: Branch
    @param root_password: Password of the server root user
    """
    with settings(user='root', password=root_password):
        run('supervisorctl restart {project_name}_{branch}_huey'.format(
            project_name=env['project_settings']['project_name'], branch=branch))
