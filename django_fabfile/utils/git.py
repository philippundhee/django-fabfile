"""
Helper methods to manage git repository
"""
from django_fabfile.utils.settings import get_remote_path
from fabric.colors import red, green
from fabric.context_managers import hide, cd, settings
from fabric.operations import local, run
from fabric.state import env
from fabric.utils import abort


def get_current_git_branch():
    """
    Returns the name of the current git branch

    @return: Name of current git branch
    """
    with hide('running', 'stdout', 'stderr'):
        return local('git rev-parse --abbrev-ref HEAD', capture=True)


def is_on_dev_branch():
    """
    Checks if is on dev branch

    @return: Boolean if on dev branch
    """
    return get_current_git_branch() == env['project_settings']['branch_name_dev']


def is_on_accpt_branch():
    """
    Checks if is on accpt branch

    @return: Boolean if on accpt branch
    """
    return get_current_git_branch() == env['project_settings']['branch_name_accpt']


def is_on_prod_branch():
    """
    Checks if on prod branch

    @return: Boolean if on prod banch
    """
    return get_current_git_branch() == env['project_settings']['branch_name_prod']


def assert_correct_branch(branch):
    """
    Aborts if given branch does not correspond with the current git branch. Only for accpt and prod

    @param branch: Branch to check
    """
    current_branch = get_current_git_branch()
    if (branch == env['project_settings']['branch_name_prod'] or branch == env['project_settings'][
        'branch_name_accpt']) and get_current_git_branch() != branch:
        abort(red('You are not on the {target}, but on the {current} branch! Something might be wrong.'.format(
            current=current_branch, target=branch)))

def assert_not_dev(branch):
    """
    Aborts if target branch is the dev branch.

    @param branch: Branch to check
    """
    if branch == env['project_settings']['branch_name_dev']:
        abort(red('This operation is not allowed to be performed for the dev branch.'))


def get_remote_origin():
    """
    Gets the remote origin config of the git repository

    @return: Remote origin
    """
    with hide('running', 'stdout', 'stderr'):
        return local('git config --get remote.origin.url', capture=True)


def git_remote_clone(branch, server_user):
    """
    Clones a git repository on the server

    @param branch: Branch
    @param server_user: Server user
    """
    with settings(user=server_user, warn_only=True):
        run('git clone -q {remote} {project_path}'.format(remote=get_remote_origin(),
                                                       project_path=get_remote_path(branch)))
        print(green('Git repository cloned on server.'))


def git_remote_checkout(branch, server_user):
    """
    Checkouts a branch on the server git repository

    @param branch: Branch
    @param server_user: Server user
    @return:
    """
    with settings(user=server_user):
        with cd(get_remote_path(branch)):
            run('git checkout {branch}'.format(branch=branch))


def assert_no_uncommitted_changes():
    """
    Aborts if there are uncommitted changes

    @return:
    """
    with settings(hide('warnings'), warn_only=True):
        result = local('git diff-index --quiet HEAD', capture=True)  # Check for uncommitted changes
    if result.return_code:
        abort(red('Uncommited changes. Please commit first.', bold=True))


def merge_branches(origin_branch, target_branch):
    """
    Merges the origin and the target branch

    @param origin_branch: Branch that serves as origin (eg. dev)
    @param target_branch: Branch that serves as the target (eg. accpt)
    """
    # Get up-to-date version of origin branch
    local('git checkout {branch}'.format(branch=origin_branch))
    local('git pull origin {branch}'.format(branch=origin_branch))
    local('git push origin {branch}'.format(branch=origin_branch))
    #Get up-to-date version of target branch
    local('git checkout {branch}'.format(branch=target_branch))
    local('git pull origin {branch}'.format(branch=target_branch))
    #Merge target with origin
    local('git merge {branch}'.format(branch=origin_branch))
    #Push target branch
    local('git push origin {branch}'.format(branch=target_branch))
    #Checkout dev branch
    local('git checkout {branch}'.format(branch='dev'))