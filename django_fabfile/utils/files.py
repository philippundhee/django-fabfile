"""
Helper methods for file related operations
"""
import datetime
from django_fabfile.utils.settings import get_server_template_path, get_remote_server_conf_path, get_server_user, get_remote_home_path, get_domain, get_remote_path, get_remote_project_path

from fabric.context_managers import quiet, settings
from fabric.operations import local, run
from fabric.state import env
from fabtools.files import upload_template
import os


def get_timestamped_filename(project_name, branch, content):
    """
    Returns a filename with the current timestamp, project_name, branch and content

    @param project_name: Project name
    @param branch: Branch
    @param content: Content e.g. media or db
    @return: Filename as string
    """
    return datetime.datetime.now().strftime(
        '{project_name}{datetime_separator}{datetime_format}{datetime_separator}{branch}{datetime_separator}{content}'.format(
            project_name=project_name,
            datetime_format=env['project_settings']['date_time_file_format'],
            branch=branch,
            content=content,
            datetime_separator=env['project_settings']['date_time_separator']))


def extract_date_from_filename(filename):
    """
    Reverse function for get_timestamped_filename. Extracts the date from a given filename
    @param filename: Filename constructed by get_timestamped_filename
    @return: Date
    """
    _, date, _, _ = filename.split(env['project_settings']['date_time_separator'])
    date = datetime.datetime.strptime(date, env['project_settings']['date_time_file_format'])
    return date


def delete_all_files_in_directory(path):
    """
    Deletes all files in a directory without deleting the directory itself
    @param path: Path to the directory
    """
    with quiet():
        local('rm -r {path}/*'.format(path=path))


def move_all_files_in_directory(target_path, origin_path):
    """
    Moves all files from an origin_path to a target_path

    @param target_path: Target path
    @param origin_path: Origin path
    """
    with quiet():
        local('mv {origin_path}/* {target_path}'.format(origin_path=origin_path, target_path=target_path))


def create_dir_if_not_exists(path):
    """
    Create a directory if it does not exist

    @param path: Path to directory
    """
    if not os.path.exists(path):
        os.makedirs(path)


def create_log_files_on_server(branch):
    """
    Creates the log directory on the server

    @param branch: Branch
    """
    log_path = os.path.join(get_remote_path(branch), 'log')

    with settings(user=get_server_user(), warn_only=True):
        run('mkdir {path}'.format(path=log_path))


def upload_allowed_hosts(branch):
    """

    @param branch:
    @return:
    """
    template_path = get_server_template_path('allowed_hosts.template')
    remote_conf_path = os.path.join(get_remote_project_path(branch), env['project_settings']['project_name'],
                                    'settings', 'allowed_hosts.py')

    server_user = get_server_user()
    replacement_dict = {
        'domain': get_domain(branch),
    }
    with settings(user=server_user):
        with settings(warn_only=True):
            run('rm {remote_conf_path}'.format(remote_conf_path=remote_conf_path))
        upload_template(template_path, remote_conf_path, context=replacement_dict, user=server_user, mkdir=True)


def upload_start_huey_script(branch):
    """
    Uploads script to start huey worker to server

    @param branch: Branch
    """
    upload_conf(branch, 'start_huey.template', 'start_huey_{branch}'.format(branch=branch), True)


def upload_start_gunicorn_script(branch):
    """
    Uploads script to start gunicorn to server

    @param branch: Branch
    """
    upload_conf(branch, 'start_gunicorn.template', 'start_gunicorn_{branch}'.format(branch=branch), True)


def upload_supervisor_huey_conf(branch):
    """
    Uploads supervisor conf for huey to server

    @param branch: Branch
    """
    upload_conf(branch, 'supervisor_huey.template', 'supervisor_huey_{branch}.conf'.format(branch=branch))


def upload_supervisor_gunicorn_conf(branch):
    """
    Uploads supervisor conf for gunicorn to server

    @param branch: Branch
    """
    upload_conf(branch, 'supervisor_gunicorn.template', 'supervisor_gunicorn_{branch}.conf'.format(branch=branch))


def upload_nginx_conf(branch):
    """
    Uploads nginx conf to server

    @param branch: Branch
    """
    upload_conf(branch, 'nginx.template', 'nginx_{branch}.conf'.format(branch=branch))


def upload_conf(branch, template_name, conf_name, is_executable=False):
    """
    Uploads a template to the server. Template has to be in the templates directory

    @param branch: Branch
    @param template_name: Name of the template
    @param conf_name: Name of the resulting file
    @param is_executable: Boolean if the file has to be executable on server
    """
    template_path = get_server_template_path(template_name)
    remote_conf_path = get_remote_server_conf_path(branch, conf_name)

    server_user = get_server_user()
    replacement_dict = {
        'project_name': env['project_settings']['project_name'],
        'home_path': get_remote_home_path(),
        'branch': branch,
        'domain': get_domain(branch),
        'server_user': server_user
    }
    with settings(user=server_user):
        with settings(warn_only=True):
            run('rm {remote_conf_path}'.format(remote_conf_path=remote_conf_path))
        #Uploads the file and does the actual replacement of the template variables
        upload_template(template_path, remote_conf_path, context=replacement_dict, user=server_user, mkdir=True)
        if is_executable:
            run('chmod +x {file}'.format(file=remote_conf_path))