"""
Helper methods for the virtualenv environment
"""
import sys


def is_virtualenv_created():
    """
    Checks if project is in a virtualenv. It doesn't matter if the virtualenv is activated or not.
    Trick from http://stackoverflow.com/questions/1871549/python-determine-if-running-inside-virtualenv

    @return Boolean if there is a virtualenv
    """
    return hasattr(sys, 'real_prefix')