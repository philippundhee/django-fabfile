"""
Settings related function
"""
import getpass
import json
import sys
from fabric.colors import red
from fabric.context_managers import settings
from fabric.operations import prompt, local
from fabric.state import env
from fabric.utils import abort
from fabric.contrib import django as django_
import os

PROJECT_SETTINGS_CONF_FILE = 'fab_project_settings.conf'


def load_project_settings():
    """
    Loads the JSON config file with the project settings. Saves loaded settings in env['project_settings']
    """
    try:
        with open(os.path.join(PROJECT_SETTINGS_CONF_FILE)) as settings_file:
            env['project_settings'] = json.load(settings_file)
            env.hosts = env['project_settings']['hosts']
    except IOError:
        abort(red('Settings file not found. Ensure executing fabric from project root directory.', bold=True))


def save_project_settings():
    """
    Saves the content of env['project_settings'] in the JSON settings config file
    """
    filename = os.path.join(get_local_path(), PROJECT_SETTINGS_CONF_FILE)
    with open(filename, 'w') as settings_file:
        json.dump(env['project_settings'], settings_file, sort_keys=True, indent=4)

    #Commit file
    with settings(warn_only=True):
        local('git add {}'.format(filename))
        local('git commit -m "Committed {}"'.format(PROJECT_SETTINGS_CONF_FILE))
        local('git push')


def get_or_prompt_setting(key, prompt_message):
    """
    Gets a setting or prompts for it if not available

    @param key: Key in the env['project_settings'] variable
    @param prompt_message: Prompt message if key is not yet available
    @return: Value of the setting
    """
    if not key in env['project_settings']:
        env['project_settings'][key] = prompt(prompt_message)
        save_project_settings()

    return env['project_settings'][key]


def get_django_settings(branch):
    """
    Get the django settings according to the given branch

    @param branch: Branch to select the correct settings file
    @return: Settings
    """
    # put the Django project on sys.path
    sys.path.insert(0, get_local_path())
    django_.settings_module(
        '{project_name}.{project_name}.settings.{branch}'.format(project_name=env['project_settings']['project_name'],
                                                                 branch=branch))

    import django.conf

    reload(django.conf)
    from django.conf import settings

    return settings


def get_db_settings(branch):
    """
    Helper utility that returns a dictionary with the most important db settings

    @param branch: Branch to select correct settings
    """
    django_settings = get_django_settings(branch)
    return {
        'username': django_settings.DATABASES['default']['USER'],
        'database': django_settings.DATABASES['default']['NAME'],
        'password': django_settings.DATABASES['default']['PASSWORD']
    }


def get_key():
    """
    Get the secret key of django

    @param branch: Branch to select correct settings
    @return: Secret key
    """
    django_settings = get_django_settings(env['project_settings']['branch_name_dev'])
    return django_settings.SECRET_KEY


def get_local_path():
    """
    Gets the path to the root project

    @return: Root project path
    """
    return env.path


def get_local_project_path():
    """
    Gets the path the django project

    @return: Django project path
    """
    return os.path.join(get_local_path(), env['project_settings']['project_name'])


def get_local_media_path():
    """
    Gets the local media path

    @return: Local media path
    """
    return os.path.join(get_local_project_path(), env['project_settings']['media_directory'])


def get_local_backup_path():
    """
    Gets the local backup path

    @return: Local backup path
    """
    return os.path.join(get_local_project_path(), env['project_settings']['tmp_backup_directory'])


def get_remote_home_path():
    """
    Path to the home directory of the server user

    @return: Home path
    """
    return get_or_prompt_setting('home_path', 'Path for user\'s home directory:')


def get_remote_path(source_branch):
    """
    Gets the path to the root project on the server

    @param source_branch: Branch
    @return: Root project path
    """
    return os.path.join(get_remote_home_path(),
                        '{project_name}project_{branch}'.format(project_name=env['project_settings']['project_name'],
                                                                branch=source_branch))


def get_remote_project_path(source_branch):
    """
    Gets the path to the django project on the server

    @param source_branch: Branch
    @return: Django project path
    """
    return os.path.join(get_remote_path(source_branch), env['project_settings']['project_name'])


def get_server_user():
    """
    Get the name of the server user

    @return: Name of server user
    """
    return get_or_prompt_setting('server_user', 'Please enter server username: ')


def get_server_root_password():
    """
    Prompts for the root user password.
    @return: Password
    """
    return getpass.getpass('Please enter password for server user "root": ')


def get_domain(branch):
    """
    Get the domain website

    @param branch: Domain for the branch
    @return: Domain
    """
    return get_or_prompt_setting('domain_{branch}'.format(branch=branch),
                                 'Please enter the domain for the {branch} (without www): '.format(branch=branch))


def get_server_template_path(name):
    """
    Gets the path to the server template directory
    @param name: Name of the template
    @return: Path
    """
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), '../', 'templates', name)


def get_remote_server_conf_path(branch, name):
    """
    Returns the path to the directory where all server configurations are saved.

    @param branch: Branch on server
    @param name: Name of the config file
    @return: Path
    """
    return os.path.join(get_remote_path(branch), 'server', name)