"""
Helper method for postgres database
"""
import getpass
from fabric.colors import red, green
from fabric.api import local, settings
from fabric.context_managers import hide, shell_env


def run_pg_command(command, password):
    """
    Runs the given psql command without password prompt.

    @param command: Command to be executed
    @param password: Password for user
    @return:
    """
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        with shell_env(PGPASSWORD=password):
            res = local(command, capture=True)
            return res


def ask_db_password(username):
    """
    Asks for the password used for the following postgres commands.
    Checks if the password is correct.

    @param username: Database user
    @return: The password
    """
    while True:
        password = getpass.getpass('Please enter the password for the postgres user "{}":'.format(username))
        res = run_pg_command('psql -U {} -c ""'.format(username), password)
        if res.succeeded:
            return password
        else:
            print(red('Error: Incorrect password', bold=True))


def pg_dump_db(username, password, database, target_file):
    """
    Creates a dump of the database

    @param username: Database user
    @param password: Password for database user
    @param database: Database
    @param target_file: File for the dump
    """
    print(green('Dump DB', bold=True))
    run_pg_command('pg_dump -Fc --no-acl --no-owner -U {username} {database} > {target_file}'.format(username=username,
                                                                                                     database=database,
                                                                                                     target_file=target_file),
                   password)


def pg_restore_db(username, password, database, source_file):
    """
    Restores a given dump

    @param username: Database user
    @param password: Password for database user
    @param database: Database
    @param source_file: File of the dump
    """
    print(green('Restore DB', bold=True))
    run_pg_command('pg_restore --clean --no-acl --no-owner -n public -U {username} -d {database} {source_file}'.format(
        username=username,
        database=database,
        source_file=source_file), password)


def create_db_user(username, password, superuser_password):
    """
    Creates a new role

    @param username: Username of the new role
    @param password: Password of the new role
    @param superuser_password: Password of the super user postgres
    """
    options = ['NOSUPERUSER', 'NOCREATEDB', 'NOCREATEROLE', 'NOINHERIT', 'LOGIN', 'ENCRYPTED']
    command = 'psql -U postgres -c "CREATE USER {username} {options}  PASSWORD \'{password}\';"'.format(
        username=username,
        options=' '.join(options),
        password=password)
    run_pg_command(command, superuser_password)


def db_user_exists(username, superuser_password):
    """
    Check if a PostgreSQL user exists.

    @param username: Username to check
    @param superuser_password: Password of super user
    @return: Boolean of user exists
    """
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        command = 'psql -U postgres -t -A -c "SELECT COUNT(*) FROM pg_user WHERE usename = \'{}\';"'.format(username)
        res = run_pg_command(command, superuser_password)
        return res == "1"


def create_database(db_name, username, superuser_password):
    """
    Create the database and make user the owner

    @param db_name: DB name to create
    @param username: Role of the owner
    @param superuser_password: Password of the super use
    """
    with settings(warn_only=True):
        while True:
            command = 'createdb --username=postgres --encoding=utf8 --template=template0 --owner={} {}'.format(username,
                                                                                                               db_name)
            result = run_pg_command(command, superuser_password)
            if result.return_code == 0:
                break
            else:
                if raw_input(
                        'Wrong password or database already existing? Type y to retype the password, n to continue. ').lower() != 'y':
                    break


def db_exists(database, superuser_password):
    """
    Check if a database exists

    @param database:
    @param superuser_password:
    @return:
    """
    #Run an empty command. If no error occurs the db exists
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        command = 'psql -U postgres -d {} -c ""'.format(database)
        res = run_pg_command(command, superuser_password)
        return res.succeeded