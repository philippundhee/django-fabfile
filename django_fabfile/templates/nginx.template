upstream %(project_name)s_%(branch)s_server {
  # fail_timeout=0 means we always retry an upstream even if it failed
  # to return a good HTTP response (in case the Unicorn master nukes a
  # single worker for timing out).

  server unix:%(home_path)s/%(project_name)sproject_%(branch)s/server/gunicorn_%(branch)s.sock fail_timeout=0;
}

server {
    listen 80;
    client_max_body_size 20M;
    server_name %(domain)s;
    rewrite ^ $scheme://www.%(domain)s$request_uri permanent;
}

server {
    listen 80;
    server_name www.%(domain)s;
    client_max_body_size 20M;
    root %(home_path)s/%(project_name)sproject_%(branch)s/%(project_name)s;

    location /media/ {
        if ($query_string) {
            expires max;
        }
    }
    location /static/ {
        if ($query_string) {
            expires max;
        }
        alias %(home_path)s/%(project_name)sproject_%(branch)s/%(project_name)s/assets/;
    }

    location / {
            proxy_connect_timeout 10;
            proxy_read_timeout 60;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $http_host;
            proxy_redirect off;

            if (!-f $request_filename) {
                proxy_pass http://%(project_name)s_%(branch)s_server;
                break;
            }
        }

    # what to serve if upstream is not available or crashes
    error_page 500 502 503 504 /media/500.html;
}