"""
Public methods for the django_fabfile
"""
import getpass

from fabric.colors import green, red
from fabric.context_managers import settings, cd
from fabric.decorators import task
from fabric.operations import run
from fabric.state import env
from fabric.utils import abort

from django_fabfile.db.postgres import ask_db_password, db_user_exists, create_db_user, db_exists, create_database
from django_fabfile.utils.files import upload_start_huey_script, upload_start_gunicorn_script, upload_supervisor_huey_conf, upload_supervisor_gunicorn_conf, upload_nginx_conf, create_log_files_on_server, upload_allowed_hosts
from django_fabfile.utils.git import assert_correct_branch, git_remote_clone, git_remote_checkout, assert_no_uncommitted_changes, merge_branches, assert_not_dev
from django_fabfile.utils.operations import abort_if_is_on_server, run_remote_fab_command, run_local_manage, get_server_user, run_remote_bootstrap, create_server_user_if_not_exists, add_deploy_key_to_bitbucket, reload_nginx, restart_gunicorn, reload_supervisor, restart_huey, run_remote_manage
from django_fabfile.utils.settings import load_project_settings, get_db_settings, save_project_settings, get_server_root_password, get_remote_path
from django_fabfile.backup.backup import backup as backup_
from django_fabfile.backup.restore import restore as restore_


def _init():
    load_project_settings()


_init()


@task
def backup(source_branch=None, media='False', db='True', is_on_server='False'):
    """
    Creates a backup for a given branch. Can backup DB and media files to an FTP server.

    @param source_branch: Branch to backup. Default is dev
    @param media: String stating if media files should be included. 'True' or 'False'
    @param db: String stating if DB should be included. 'True' or 'False'
    @param is_on_server: Do not change manually. Is set to true when executed on server.
    """
    source_branch = source_branch or env['project_settings']['branch_name_dev']
    db = db == 'True'
    media = media == 'True'
    is_on_server = is_on_server == 'True'

    if source_branch == env['project_settings']['branch_name_dev']:
        abort_if_is_on_server(is_on_server)
        backup_(source_branch, media, db)
    else:
        if is_on_server:
            backup_(source_branch, media, db)
        else:
            command = 'backup:source_branch={source_branch},media={media},db={db},is_on_server=True'.format(
                source_branch=source_branch, media=media, db=db)
            run_remote_fab_command(command, source_branch)


@task
def restore(source_branch=None, target_branch=None, media='False', db='True',
            is_on_server='False'):
    """
    Restores a backup from a given source branch to a given target branch. Can restore DB and media files from
    a FTP server.

    @param source_branch: Branch from which the dump should be taken. Default is dev
    @param target_branch: Branch which content should be replaced. Default is dev
    @param media: String stating if media files should be included. 'True' or 'False'
    @param db: String stating if DB should be included. 'True' or 'False'
    @param is_on_server: Do not change manually. Is set to true when executed on server.
    """
    source_branch = source_branch or env['project_settings']['branch_name_dev']
    target_branch = target_branch or env['project_settings']['branch_name_dev']
    db = db == 'True'
    media = media == 'True'
    is_on_server = is_on_server == 'True'

    if target_branch == env['project_settings']['branch_name_dev']:
        abort_if_is_on_server(is_on_server)
        restore_(source_branch, target_branch, media, db)
    else:
        if is_on_server:
            restore_(source_branch, target_branch, media, db)
        else:
            command = 'restore:source_branch={source},target_branch={target},media={media},db={db},is_on_server=True'.format(
                source=source_branch, target=target_branch, media=media, db=db)
            run_remote_fab_command(command, target_branch)


@task
def deploy(target_branch):
    """
    Deploys the changes from the 'lower' branch in the hierarchy to the target branch. The origin branch is chosen
    accordingly. Pulls the new changes into the target branch on the server.

    @param target_branch: Branch where the changes should be merged into.
    """
    assert_correct_branch(env['project_settings']['branch_name_dev'])

    assert_no_uncommitted_changes()

    root_password = get_server_root_password()

    # Decide what the origin branch is, based on the target branch. Origin branch is always branch one 'earlier'
    # in deployment process
    origin_branch = None
    if target_branch == env['project_settings']['branch_name_accpt']:
        origin_branch = env['project_settings']['branch_name_dev']
    elif target_branch == env['project_settings']['branch_name_prod']:
        origin_branch = env['project_settings']['branch_name_accpt']
    else:
        abort(red('Invalid target branch!', bold=True))

    merge_branches(origin_branch, target_branch)

    # Pull target branch on server
    with settings(user=get_server_user()):
        with cd(get_remote_path(target_branch)):
            run('git pull origin {branch}'.format(branch=target_branch))

    # Run the bootstrap script on the server to install new requirements
    run_remote_bootstrap(target_branch)

    # Run the migrations on the server
    run_remote_fab_command('migrate:{branch}'.format(branch=target_branch), target_branch)

    # Collect static
    run_remote_manage(target_branch, 'collectstatic --noinput')

    #Reload supervisor and nginx (Necessary if this is the first deploy) and restart the site
    reload_nginx(root_password)
    reload_supervisor(root_password)
    restart_site(target_branch, root_password)


@task
def setup_database(branch=None):
    """
    Creates the database and the user specified in the settings file

    @param branch: Branch. Default is dev
    """
    branch = branch or env['project_settings']['branch_name_dev']
    assert_correct_branch(branch)

    superuser_password = ''

    db_settings = get_db_settings(branch)

    username = db_settings['username']
    password = db_settings['password']
    database = db_settings['database']

    #Create the role (if not exists)
    if not db_user_exists(username, superuser_password):
        create_db_user(username, password, superuser_password)
        print(green('Created user {}'.format(username), bold=True))
    else:
        print(green('User {} already exists'.format(username), bold=True))

    #Create the db (if not exists)
    if not db_exists(database, superuser_password):
        create_database(database, username, superuser_password)
        print(green('Created database {}'.format(database), bold=True))
    else:
        print(green('Database {} already exists'.format(database), bold=True))


@task
def migrate(branch=None):
    """
    Runs the migrations on the database and calls syncdb for apps that don't use django-south

    @param branch: Branch. Default is dev.
    """
    branch = branch or env['project_settings']['branch_name_dev']
    assert_correct_branch(branch)

    #Switch to the directory where manage.py is
    run_local_manage(branch, 'syncdb')
    run_local_manage(branch, 'migrate')


@task
def setup_server(branch):
    """
    Sets up the accpt or prod branch on the server

    @param branch: Branch
    """
    assert_correct_branch('dev')
    assert_not_dev(branch)

    # Get password for server root user
    root_password = get_server_root_password()

    # Gets the user name on the server (or prompts if not yet set)
    server_user = get_server_user()

    # creates the server user if not yet existing
    create_server_user_if_not_exists(root_password, server_user)

    bitbucket_password = getpass.getpass(
        'Please enter password for bitbucket user "{}": '.format(env['project_settings']['bitbucket_user']))

    # add the ssh public key of the server to the deploy keys on Bitbucket
    add_deploy_key_to_bitbucket(bitbucket_password)

    # clone the git repository
    git_remote_clone(branch, server_user)

    # checkout branch
    git_remote_checkout(branch, server_user)

    # create log directory
    create_log_files_on_server(branch)

    # run the bootstrap process on the server
    run_remote_bootstrap(branch)

    # upload all server configs
    upload_server_configs(branch, root_password, reload_servers=False)

    # setup the database
    run_remote_fab_command('setup_database:{branch}'.format(branch=branch), branch)

    # restore the dev backup on the server
    restore(target_branch=branch, media='True')

    # run the migrations
    run_remote_fab_command('migrate:{branch}'.format(branch=branch), branch)
    print(green('Ran migrations'))

    # collect static
    run_remote_manage(branch, 'collectstatic --noinput --verbosity=0')
    print(green('Ran collectstatic'))

    reload_nginx(root_password)
    reload_supervisor(root_password)

    # save all new settings
    save_project_settings()
    print(green('Saved project settings'))


@task
def upload_server_configs(branch, root_password=None, reload_servers=True):
    """
    Creates and uploads the configuration files for the server

    @param branch: Branch
    @param root_password: Password for root user. Prompts if not given
    """
    assert_not_dev(branch)

    root_password = root_password or get_server_root_password()

    upload_allowed_hosts(branch)
    print(green('Uploaded allowed hosts'))

    upload_start_gunicorn_script(branch)
    print(green('Uploaded start gunicorn script'))

    upload_supervisor_gunicorn_conf(branch)
    print(green('Uploaded supervisor gunicorn conf'))

    upload_nginx_conf(branch)
    print(green('Uploaded nginx conf'))

    upload_start_huey_script(branch)
    print(green('Uploaded start huey script'))

    upload_supervisor_huey_conf(branch)
    print(green('Uploaded supervisor huey conf'))

    print(green('Uploaded all server configs'))

    if reload_servers:
        reload_nginx(root_password)
        reload_supervisor(root_password)


@task
def restart_site(branch, root_password=None):
    """
    Restarts gunicorn and huey processes

    @param branch: Branch
    @param root_password: Password for root user. Prompts if not given
    """
    assert_not_dev(branch)
    root_password = root_password or get_server_root_password()
    restart_gunicorn(branch, root_password)
    restart_huey(branch, root_password)